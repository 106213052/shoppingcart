<?php
require_once("dbconfig.php");

function getUserProfile($ID, $pwd){
	global $db;
	$sql = "SELECT name, role FROM user WHERE uID=? and password=?";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "ss", $ID, $pwd); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
	$result = mysqli_stmt_get_result($stmt); //get the results
	if ($row = mysqli_fetch_assoc($result)) {
		$ret=array('uID' => $ID, 'uName' => $row['name'], 'uRole' => $row['role']);
	} else
		return NULL;
	return $ret;
}

function checkIDPwd($ID, $passWord)
{
	global $db;
	$isValid = false;
	$sql = "SELECT count(*) C FROM user WHERE uID=? and password=?";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "ss", $ID, $passWord); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
	$result = mysqli_stmt_get_result($stmt); //get the results

	if ($row = mysqli_fetch_assoc($result)) {
		
		if ($row['C'] == 1) {
			//keep the user ID in session as a mark of login
			//$_SESSION['uID'] = $row['id'];
			//provide a link to the message list UI
			$isValid = true;
		}
	}
	return $isValid;
}


function getUserStatus($ID)
{
	$status = "You guess!!";
	return $status;
}

function addUser($ID, $passWord, $name, $role = 0)
{
	global $db;

	$sql = "SELECT * FROM user WHERE uID=? ";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "s", $ID); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
	$result = mysqli_stmt_get_result($stmt); //get the results
	while ($row = mysqli_fetch_assoc($result)) {
		if ($row['ID'] == $ID)
			return -1; // this ID is already used
	}
	// echo "$ID, $passWord, $name, $role";
	$sql = "INSERT INTO `user` (`uID`, `password`, `name`, `role`) VALUES (?, ?, ?, ?)";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "sssi", $ID, $passWord, $name, $role); //bind parameters with variables
	$result = mysqli_stmt_execute($stmt);  //執行SQL
	if ($result == false)
		return 1; // there's something wrong when adding user
	return 0;
}
