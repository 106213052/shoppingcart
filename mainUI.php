<?php
session_start();
require("productModel.php");

if (!isset($_SESSION['loginProfile'])) {
	// if not logged in, redirect page to loginUI.php
	header("Location: loginUI.php");
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Basic HTML Examples</title>
</head>

<body>
	<p>這裡是主畫面~~
		[<a href="logout.php">登出</a>]
	</p>
	<hr>
	<?php
	echo "Hello ", $_SESSION["loginProfile"]["uName"],
		", 你的ID是: ",
		$_SESSION["loginProfile"]["uID"],
		", 你的角色是: ";
	if ($_SESSION["loginProfile"]["uRole"] == 0)
		echo " 會員<HR>";
	else
		echo " 員工<HR>";
	$result = getProductList();
	?>
	<br>
	<a href="orderView.php" target="_self">列出我的訂單</a>
	<a href="cartView.php" target="_self">顯示我的購物車</a>
	<br>
	<table width="400" border="2">
		<tr>
			<td>id</td>
			<td>產品名</td>
			<td>價格</td>
			<td>加入購物車</td>
			<td>備註</td>
		</tr>
		<?php
		while ($rs = mysqli_fetch_assoc($result)) {
			echo "<tr><td>" . $rs['prdID'] . "</td>";
			echo "<td>{$rs['name']}</td>";
			echo "<td>", $rs['price'], "</td>";
			// echo "addToCart.php?prdID='" . $rs['prdID'] . "<br>";
			// echo "<td><a href='addToCart.php?prdID=" . $rs['prdID'] . "' target='_self'>Add</a></td>";
			echo "<td><a href='cartControl.php?act=add&prdID=" . $rs['prdID'] . "' target='_self'>+</a></td>";
			echo "<td>", $rs['detail'], "</td>";
			echo "</tr>";
		}
		?>
	</table>

	<script>
		function includeHTML() {
			var z, i, elmnt, file, xhttp;
			/*loop through a collection of all HTML elements:*/
			z = document.getElementsByTagName("*");
			for (i = 0; i < z.length; i++) {
				elmnt = z[i];
				/*search for elements with a certain atrribute:*/
				file = elmnt.getAttribute("w3-include-html");
				if (file) {
					/*make an HTTP request using the attribute value as the file name:*/
					xhttp = new XMLHttpRequest();
					xhttp.onreadystatechange = function() {
						if (this.readyState == 4) {
							if (this.status == 200) {
								elmnt.innerHTML = this.responseText;
							}
							if (this.status == 404) {
								elmnt.innerHTML = "Page not found.";
							}
							/*remove the attribute, and call this function once more:*/
							elmnt.removeAttribute("w3-include-html");
							includeHTML();
						}
					}
					xhttp.open("GET", file, true);
					xhttp.send();
					/*exit the function:*/
					return;
				}
			}
		};
	</script>


	<?php
	if (isset($_GET['act'])) {
		$act = $_GET['act'];
		if ($act == "addToCart") {
			echo "<script>alert('Add to Cart successfully!')</script>";
		} else if ($act == "checkout") {
			echo "<script>alert('Checkout successfully!')</script>";
		}
	}
	?>


</body>

</html>